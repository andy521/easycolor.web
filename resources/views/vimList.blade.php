@extends('layout.app')
@section('title','vim_配色方案_主题_下载_easycolor')
@section('head')
    <meta name="keywords" content="vim,配色方案,主题">
    <meta name="description" content="提供400多款vim配色方案，总有一款是你想要的">
    <style type="text/css">
        .code span{
            font-family: consolas;
        }

        pre{
            margin:0;
            width: 100em;
            border-radius:0px;
        }
        .Normal{
            border:none;
            font-family: "DejaVu Sans Mono", monospace;
        }
        .vim-git{
            float:right;
            color:#333;
        }
        a{
            text-decoration: none;
        }
    </style>
@endsection
@section('content')
<div style="text-align: center;margin:1em;">
    <img class="brand-img" src="/img/vim-logo.png"/>
</div>
@include('part.searchForm')
<ul id="vim-ul"> </ul>
<div id="pager"></div>
<script type="text/javascript">
    var idx = 1;
    var template =
        "<li>\n" +
        "<a style='text-decoration: none;' href='/vim/detail/{id}.html'> " +
        "<div class='vim-name'>{name}</div>\n" +
        "{example} " +
        "</a>\n" +
        "</li>\n";

    function find(idx) {
        var option = pickEntity('sf');
        if(nullOrEmpty(idx))
            option.index = 1;
        else
            option.index = idx;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/vim/find',
            type:'POST',
            data:option,
            success:function (resp) {
                renderList(resp.data,'vim-ul',template);
                renderPageBar(resp.page,'pager','find');
                idx = option.index;
            }
        });
    }

    $(function(){
        find();
    });
</script>
@endsection
