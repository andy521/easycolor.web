{{--google analytics--}}
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-103847606-1', 'auto');
    ga('send', 'pageview');
</script>

{{--baidu analytics--}}
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?40a6d80bc154fff44c8981c765c747aa";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

{{--cnzz analytics--}}
<div style="display: none">
    <script src="http://s11.cnzz.com/z_stat.php?id=1260867660&web_id=1260867660" language="JavaScript"></script>
</div>