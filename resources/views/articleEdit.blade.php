@extends('layout.app')
@section('title','文章编辑')
@section('head')
    <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
@endsection
@section('content')
    <div style="padding:3em;">
        id:<input id="id" name="id"/>
        <br/>
        title:<input id="title" name="title"/>
        <br/>
        type_id:<input id="type_id" name="type_id"/>
        <br/>
        content:<textarea id="content" name="content"></textarea>
        <br/>
        <span class="ybtn" onclick="edit()">save</span>
        <br/>
    </div>
    <script type="text/javascript">
        var bean = {!! $bean !!};
        //edit
        function edit() {
            var data={};
            if(!nullOrEmpty(val('id')))
                data.id = val('id');
            data.content = editor.getData();
            data.title = val('title');
            data.type_id= val('type_id');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'/article/edit',
                type:'POST',
                data:data,
                success:function (r) {
                    if(!r)
                        alert('失败');
                    else
                        alert('成功');
                }
            });
        }
        var editor;
        $(function(){
            editor = getEditor('content','/ckeditor/uploadImg');
            if(!nullOrEmpty(bean)){
                val('id',bean.id);
                val('type_id',bean.type_id);
                val('title',bean.title);
                editor.setData(bean.content);
            }
        });
    </script>
@endsection

