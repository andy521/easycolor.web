@extends('layout.app')
@section('head')
    <title>idea_主题_配色方案_下载_{{$bean->name}}_easycolor</title>
    <meta name="keywords" content="intelliJ idea,配色方案,主题,{{$bean->name}}">
    <meta name="description" content="intelliJ idea配色方案{{$bean->name}}下载">
    <script type="text/javascript" src="/js/embed.js" charset="UTF-8"></script>
    <style type="text/css" >
        .title{
            font-size: 1.5em;
            padding:.7em 1em;
        }
    </style>
@endsection
@section('content')
<div class="container relative">
    <h1>
        <img class="detail-logo" src="/img/jetbrains.png"/>
        {{ $bean->name }}
        <a href="/download/idea/{{$bean->download_link}}" class="ybtn"> 点击下载 </a>
    </h1>
</div>
<div style="clear:both;"></div>
<div style="width:80%;margin:0 auto;">
    <div class="title">代码演示</div>
    <div class="code" style='background: {{$bean->color}};'>
       {!! $bean->example !!}
    </div>
    </div>
    <div style="height: 2em;"></div>
@endsection
