@extends('layout.app')
@section('title','vim_idea_eclipse_visual studio_配色方案_主题_下载')
@section('head')
    <meta name="keywords" content="idea配色方案,vim配色方案,eclipse配色方案,visual stutio配色方案,配色方案,idea主题,eclipse主题，vim主题">
    <meta name="description" content="easycolor专注于提供各种开发环境的配色方案和主题的整理，为开发者提供一个查找和下载配色方案的高效解决方案">
    <link href="/css/themes.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        #bottomNav {
            background-color: #ececec;
            border-top: 1px solid #b9b9b9;
            z-index: 999;
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            _position: absolute;
            /*_top: expression_r(documentElement.scrollTop + documentElement.clientHeight-this.offsetHeight);*/
            overflow: visible;
        }

        .content {
            width: 900px;
            margin: 0 auto;
            list-style: none;
        }

        .content li {
            float: left;
            margin: .2em;
        }

        .box {
            text-align: center;
            border: 1px solid #b9b9b9;
            -webkit-border-radius: 8px;
            padding-bottom: 1em;
        }

        .box:hover {
            background-color: #f7f7f7;
        }

        .brand {
            width: 200px;
        }

        a {
            color: black;
            text-decoration: none;
        }

        .code {
            border: none;
        }

        .zhanzhang {
            position: fixed;
            bottom: 0;
            width: 100%;
            margin: 0 auto;
            text-align: center;
            display: none;
        }
    </style>

@endsection

@section('content')
    <div style='text-align:center;padding:.8em;font-family:"微软雅黑";font-size:3em;'>你想要的色彩，都在这里</div>
    <ul class="content">
        <li>
            <div style='float:left;' class='box'>
                <a href="/intelliJidea/list.html">
                    <img alt="下载idea配色方案" class='brand' src='/img/jetbrains.png'/>
                    <div>下载idea配色方案</div>
                </a>
            </div>
        </li>
        <li>
            <div style='float:right' class='box'>
                <a href="/vim/list.html">
                    <img alt="下载vim配色方案" class='brand' src='/img/vim-logo.png'/>
                    <div>下载vim配色方案</div>
                </a>
            </div>
        </li>
        <li>
            <div style='float:right' class='box'>
                <a href="/eclipse/list.html">
                    <img class='brand' src='/img/eclipse.png'/>
                    <div>下载eclipse配色方案</div>
                </a>
            </div>
        </li>
        <li>
            <div style='float:right' class='box'>
                <a href="">
                    <img alt="下载visual stutio配色方案" class='brand' src='/img/visual-studio.png'/>
                    <div>Comming Soon ...</div>
                </a>
            </div>
        </li>
    </ul>
@endsection


