{{--应用布局页--}}
<html>
<head>
    <title> @yield('title') </title>
    @yield('head')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="Copyright" content="easycolor(www.easycolor.cc)" />
    <meta name="author" content="yanwushu" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/css/global.css" type="text/css" rel="stylesheet">
    <link href="/css/themes.css" type="text/css" rel="stylesheet">
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/commons.js"></script>
    <script src="/js/biz.js"></script>
</head>
<body>
@include('part.header')
<div style="width:75%;margin:0 auto;padding-bottom: 8em;">
    @yield('content')
    <div style="clear:both;"></div>
    {{--<div style="display: flex;justify-content:center;padding:5em;">--}}
    <div style="padding:1em;text-align: center;border: 1px solid #b9b9b9;border-radius: .3em; width: 75%; margin: 1em auto;">
        <div style="text-align: center;font-weight:bold;color:#007cea;padding:.3em;"> 本项目现已开源：<a href='https://gitee.com/yanwushu/easycolor.web'>[https://gitee.com/yanwushu/easycolor.web]</a>欢迎大家参与</div>
        <div style="text-align: center;font-weight:bold;color:#007cea;padding:.3em;"> qq交流群：946559471</div>
        <br/>
        <div style="text-align: center;">EasyColor上线以来已经服务数以万计的开发者，并且为开发者提供了数十万次的下载。</div>
        <div style="text-align: center;">EasyColor由作者在业余时间开发，由于囊肿羞涩，无法长时间支付服务器和域名费用，这里不得已挂出微信收钱码，希望能够得到一点捐助。</div>
        <div style="text-align: center;color:red;font-weight: bold;">如果未能募集到足够的捐款，EasyColor将会在数月之内关闭服务，敬请谅解。</div>
        <div style="text-align: center;color: #007cea;font-weight: bold;">如果不想看到捐助二维码，请使用adblock等工具将其屏蔽，作者对可能造成的困扰致歉。</div>
        <div style="height: 2em;"></div>
        <div style="display:flex;justify-content: center;">
            <div>
                <img src="/img/0.99.png" style="width:150px;height: 170px;"/>
                <div style="color: #007cea;font-weight: bold;">向开发者献￥0.99的爱心</div>
            </div>
            <div style="width:30px;"></div>
            <div>
                <div style="height: 20px;"></div>
                <img src="/img/qrcode.png" style="width:150px;height: 150px;"/>
                <div style="color: #ff9b00;font-weight: bold;">土豪专用，任意金额，臣妾谢恩</div>
            </div>
        </div>
    </div>
    {{--</div>--}}
    {{--<div class="ad-box">--}}
        {{--@include('part.sogouAd')--}}
    {{--</div>--}}
</div>
@include('part.footer')
@include('part.analytics')
</body>
</html>