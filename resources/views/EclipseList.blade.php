@extends('layout.app')
@section('title','eclipse_配色方案_主题_下载_easycolor')
@section('head')
    <meta name="keywords" content="eclipse,配色方案,主题">
    <meta name="description" content="提供3000多款eclipse配色方案及其下载文件，总有一款是你想要的">
    <link href="/css/eclipse_style.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .line{
            font-family: consolas;
        }
        body{
            margin-bottom: 60px;
            margin-top: 50px;
        }
        .default {
            font-family: consolas;
            font-weight: normal;
        }

        pre {
            margin: 0;
            width: 100em;
            border-radius: 0px;
        }
        .class{
            padding: 0 .5em;
        }
        .method{
            padding: 0 .5em;
        }
    </style>
@endsection
@section('content')
<div style="text-align: center;margin:1em;">
    <img class="brand-img" src="/img/eclipse.png"/>
</div>
@include('part.searchForm')
<ul id="vim-ul"> </ul>
<div id="pager"></div>
<script type="text/javascript">
    var idx = 1;
    var template = "<li>\n" +
        " <a href='/eclipse/detail/{id}.html'>   " +
        " <div class='vim-name'>\n" +
        "         {name}\n" +
        "    </div>\n" +
        "    <style type='text/css'>\n" +
        "        {example}\n" +
        "    </style>\n" +
        "    <div id='theme-{download_link}'>\n" +
        "            <div class='view'>\n" +
        "                <div class='editor java' style='padding: 10px 7px;'>\n" +
        "                    <div class='default'>\n" +
        "                        <div class='line'><span class='keyword'>public class</span> <span\n" +
        "                                    class='class'>Demo</span> {\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;<span\n" +
        "                                    class='keyword'>private static final</span> <span class='class'>String</span>\n" +
        "                            <span class='staticfinalfield'>CONSTANT</span> <span class='operator'>=</span>\n" +
        "                            <span\n" +
        "                                    class='string'>'String'</span>;\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;<span class='keyword'>private</span> <span\n" +
        "                                    class='class'>Object</span> <span class='field'>o</span>;\n" +
        "                        </div>\n" +
        "                        <div class='line'></div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;<span class='javadoc'>/**</span></div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='javadoc'>* Creates a new demo.</span>\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='javadoc'>* <span\n" +
        "                                        class='javadoctag'>@param</span>                                    o The object to demonstrate.</span>\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='javadoc'>*/</span></div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;<span class='keyword'>public</span> Demo(<span\n" +
        "                                    class='class'>Object</span> <span class='parametervariable'>o</span>) {\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='keyword'>this</span>.<span\n" +
        "                                    class='field'>o</span> = <span class='parametervariable'>o</span>;\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='class'>String</span>\n" +
        "                            <span class='localvariabledeclaration'>s</span> <span class='operator'>=</span>\n" +
        "                            <span class='staticfinalfield'>CONSTANT</span>;\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='keyword'>int</span>\n" +
        "                            <span class='localvariabledeclaration'>i</span> <span class='operator'>=</span>\n" +
        "                            <span class='number'>1</span>;\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;}</div>\n" +
        "                        <div class='line'></div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;<span\n" +
        "                                    class='keyword'>public static void</span> <span\n" +
        "                                    class='methoddeclaration'>main</span>(\n" +
        "                            <span\n" +
        "                                    class='class'>String</span>[] <span class='parametervariable'>args</span>) {\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='class'>Demo</span>\n" +
        "                            <span class='localvariabledeclaration'>demo</span> <span class='operator'>=</span>\n" +
        "                            <span class='keyword'>new</span> <span class='method'>Demo</span>();\n" +
        "                        </div>\n" +
        "                        <div class='line'>&nbsp;&nbsp;&nbsp;&nbsp;}</div>\n" +
        "                        <div class='line'>}</div>\n" +
        "                    </div>\n" +
        "                </div>\n" +
        "                <div style='clear:both;'></div>\n" +
        "            </div>\n" +
        "    </div>\n" +
        " </a>"+
        "</li>";

    function find(idx) {
        var option = pickEntity('sf');
        if(nullOrEmpty(idx))
            option.index = 1;
        else
            option.index = idx;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/eclipse/find',
            type:'POST',
            data:option,
            success:function (resp) {
                renderList(resp.data,'vim-ul',template);
                renderPageBar(resp.page,'pager','find');
                idx = option.index;
            }
        });
    }

    $(function(){
        find();
    });
</script>
@endsection
