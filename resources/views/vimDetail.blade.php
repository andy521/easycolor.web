@extends('layout.app')
<title>idea_主题_配色方案_下载_{{$bean->name}}_easycolor</title>
@section('head')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>vim_配色方案_主题_{{$bean->name}}_easycolor</title>
    <meta name="keywords" content="vim配色方案,{{$bean->name}}">
    <meta name="description" content="vim配色方案{{$bean->name}}下载">
    <style type="text/css" >
        .title{
            font-size: 1.5em;
            padding:.7em 1em;
        }
    </style>
@endsection
@section('content')
<div style="margin:0 auto;width:80%;">
    <h1>
        <img class="detail-logo" src="/img/vim-logo.png"/>
        {{$bean->name}} <a href="{{$bean->download_link}}" target="_blank" class="ybtn">
            到github下载配色方案
        </a>
    </h1>
    <div style="clear:both;"></div>
    <div class="title">代码演示</div>
    {!!$bean->example!!}
</div>
@endsection
