@extends('layout.app')
@section('head')
    <title>{{$bean->title}}</title>
    <style type="text/css">
        h2{
            font-size: .8em;
        }
    </style>
@endsection
@section('content')
    <h1 class="article-title"> {{$bean->title}} </h1>
    <div class="article-box">
        {!! $bean->content !!}
    </div>
    <div style="height: 5em;"></div>
@endsection

