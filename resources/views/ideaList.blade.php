@extends('layout.app')
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
@section('title','idea_配色方案_主题_下载_easycolor')
@section('head')
    <meta name="keywords" content="idea,配色方案,主题">
    <meta name="description" content="提供丰富的idea配色方案及其下载文件，总有一款你想要的">
    {{--<script src="__PUBLIC__/js/ca-pub-2766284746185215.js"></script>--}}
    <script src="/js/hm.js"></script>
    <style>
              .code{
              border:none;
              }
              .ideas{
              list-style:none;
              width: 600px;
              padding-bottom: 1em;
              margin: 0 auto;
              }
              .ideas li {
              float:left;
              padding:.2em;
              background: #f6f6f6;
              border:1px dashed #ddd;
              margin:.3em;
              }

              #idea-ul{
              list-style: none;
              overflow: hidden;
              }
              #idea-ul li{
                  width: 22%;
                  overflow: hidden;
                  height: 20em;
                  margin: .3em;
                  float: left;
                  background: #f5f5f5;
                  border: 1px solid #ddd;
                  border-radius: 4px;
              }
              </style>
    <style type="text/css">
                              .preview {
                              white-space: pre;
                              overflow: hidden;
                              font-family: "Source Code Pro", monospace;
                              font-size: 1em;
                              }
                              .preview > * {
                              display: block;
                              }
                              </style>
    <style type="text/css">
                              #theme-view .wrapper {
                              display: -webkit-flex;
                              display: flex;
                              -webkit-flex-direction: row;
                              flex-direction: row;
                              -webkit-flex-wrap: nowrap;
                              flex-wrap: nowrap;
                              -webkit-justify-content: flex-start;
                              justify-content: flex-start;
                              -webkit-align-items: flex-start;
                              align-items: flex-start;
                              -webkit-align-content: flex-start;
                              align-content: flex-start;
                              }
                              #theme-view .wrapper .spacer {
                              -webkit-flex-grow: 1;
                              flex-grow: 1;
                              -webkit-flex-shrink: 0;
                              flex-shrink: 0;
                              -webkit-flex-basis: auto;
                              flex-basis: auto;
                              }
                              #theme-view .wrapper .content {
                              -webkit-flex-grow: 1;
                              flex-grow: 1;
                              -webkit-flex-shrink: 1;
                              flex-shrink: 1;
                              -webkit-flex-basis: 40em;
                              flex-basis: 40em;
                              padding: 0 1em 1em 1em;
                              }
                              #theme-view .wrapper .content h1 {
                              font-size: 2em;
                              margin-top: 1em;
                              font-weight: normal;
                              }
                              #theme-view .wrapper .content h2 {
                              font-size: 1.2em;
                              margin-top: 1.2em;
                              margin-bottom: 0.2em;
                              font-weight: normal;
                              }
                              #theme-view .wrapper .content .preview > * {
                              padding: 1em 0;
                              }
                              #theme-view .wrapper .content .comment,
                              #theme-view .wrapper .content .author {
                              opacity: .7;
                              border-left: 4px solid;
                              font-style: italic;
                              padding: .5em 0 0 1em;
                              margin: 0 0 0 .1em;
                              }
                              #theme-view .wrapper .content .author {
                              padding: 0 0 .5em 1em;
                              }
                              </style>
@endsection
@section('content')
    <div style="text-align: center;margin:1em;">
        <img class="brand-img" src="/img/jetbrains.png"/>
    </div>
    <div style="font-size:1.1em;text-align: center;">支持InteliJ IDEA 、PhpStorm、 PyCharm、 RubyMine、 WebStorm、 AppCode 等所有JetBrains家族ide。 </div>
    @include('part.searchForm')
    <ul id="idea-ul"></ul>
    <div id="pager"></div>
<div class="clear:both;"></div>
<script type="text/javascript">
    var idx = 1;
    var template =
        "<li >\n" +
        "<a href='/intelliJidea/detail/{id}.html' class='no-uderline'>\n" +
        "<div class='vim-name'> {name} </div>\n" +
        "    <div class='preview' style='background:{color}'>{example}\n" +
        "            <div class='info'>\n" +
        "                <div class='downloads-wrapper'>\n" +
        "                    {download_count} <span class='glyphicon glyphicon-download-alt'></span>\n" +
        "                </div>\n" +
        "            </div>\n" +
        "    </div>\n" +
        "</a>\n" +
        "</li>";

    function find(idx) {
        var option = pickEntity('sf');
        if(nullOrEmpty(idx))
            option.index = 1;
        else
            option.index = idx;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:'/intelliJidea/find',
            type:'POST',
            data:option,
            success:function (resp) {
                renderList(resp.data,'idea-ul',template);
                renderPageBar(resp.page,'pager','find');
                idx = option.index;
            }
        });
    }

    $(function(){
        find();
    });
</script>
@endsection
