@extends('layout.app')
@section('head')
    <title>{$typename}_{$row.name}_配色方案_主题_easycolor</title>
    <meta name="keywords" content="{$typename},配色方案,{$row.name}">
    <meta name="description" content="{$typename}配色方案{$row.name}下载">
    <link href="/css/eclipse_style.css" type="text/css" rel="stylesheet">
    <script src="/js/hm.js"></script>
    <script type="text/javascript" async="" src="/js/embed.js" charset="UTF-8"></script>
    <style type="text/css">
        .line {
            font-family: consolas;
            font-weight: normal;
        }
        .view{
            border-radius: 5px;
        }
        .title{
            font-size: 1.5em;
            padding:.7em 0;
        }
        .class{
            padding: 0 .5em;
        }
        .method{
            padding: 0 .5em;
        }
    </style>
    <style type="text/css">
        @foreach ($demos as $demo)
            {!! $demo->content !!}
        @endforeach
    </style>
@endsection
@section('content')
<div style="width:80%;margin:0 auto;">
    <h1>
        <img class="detail-logo" style="float:left;" src="/img/eclipse.png"/> {{$bean->name}}
        <a href="/download/eclipse/xml/{{$bean->name}}/{{$bean->download_link}}" target="_blank" class="ybtn"> 点击下载(XML) </a>
        &nbsp;
        <a href="/download/eclipse/epf/{{$bean->name}}/{{$bean->download_link}}" target="_blank" class="ybtn"> 点击下载(EPF) </a>
    </h1>
    <!--java-->
    <div class="title">Java代码演示</div>
    <div class="view">
                 <div class="sidebar">
                 <div class="lineNumber">0</div>
                 <div class="lineNumber">1</div>
                 <div class="lineNumber">2</div>
                 <div class="lineNumber">3</div>
                 <div class="lineNumber">4</div>
                 <div class="lineNumber">5</div>
                 <div class="lineNumber">6</div>
                 <div class="lineNumber">7</div>
                 <div class="lineNumber">8</div>
                 <div class="lineNumber">9</div>
                 <div class="lineNumber">10</div>
                 <div class="lineNumber">11</div>
                 <div class="lineNumber">12</div>
                 <div class="lineNumber">13</div>
                 <div class="lineNumber">14</div>
                 <div class="lineNumber">15</div>
                 <div class="lineNumber">16</div>
                 <div class="lineNumber">17</div>
                 <div class="lineNumber">18</div>
                 </div>
                 <div class="editor java">
                 <div class="default">
                 <div class="line"><span class="keyword">public class</span> <span class="class">Demo</span> {</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">private static final</span> <span class="class">String</span> <span class="staticfinalfield">CONSTANT</span> <span class="operator">=</span> <span class="string">"String"</span>;</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">private</span> <span class="class">Object</span> <span class="field">o</span>;</div>
                 <div class="line"></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="javadoc">/**</span></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="javadoc">* Creates a new demo.</span></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="javadoc">* <span class="javadoctag">@param</span> o The object to demonstrate.</span></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="javadoc">*/</span></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">public</span> Demo(<span class="class">Object</span> <span class="parametervariable">o</span>) {</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">this</span>.<span class="field">o</span> = <span class="parametervariable">o</span>;</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="class">String</span> <span class="localvariabledeclaration">s</span> <span class="operator">=</span> <span class="staticfinalfield">CONSTANT</span>;</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">int</span> <span class="localvariabledeclaration">i</span> <span class="operator">=</span> <span class="number">1</span>;</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;}</div>
                 <div class="line"></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">public static void</span> <span class="methoddeclaration">main</span>(<span class="class">String</span>[] <span class="parametervariable">args</span>) {</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="class">Demo</span> <span class="localvariabledeclaration">demo</span> <span class="operator">=</span> <span class="keyword">new</span> <span class="method">Demo</span>();</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;}</div>
                 <div class="line">}</div>
                 </div>
                 </div>
                 <div class="clear"></div>
                 </div>
    <!--php-->
    <div class="title"> php代码演示</div>
    <div class="view">
                 <div class="sidebar">
                 <div class="lineNumber">0</div>
                 <div class="lineNumber">1</div>
                 <div class="lineNumber">2</div>
                 <div class="lineNumber">3</div>
                 <div class="lineNumber">4</div>
                 <div class="lineNumber">5</div>
                 <div class="lineNumber">6</div>
                 <div class="lineNumber">7</div>
                 <div class="lineNumber">8</div>
                 <div class="lineNumber">9</div>
                 <div class="lineNumber">10</div>
                 <div class="lineNumber">11</div>
                 <div class="lineNumber">12</div>
                 <div class="lineNumber">13</div>
                 <div class="lineNumber">14</div>
                 <div class="lineNumber">15</div>
                 <div class="lineNumber">16</div>
                 <div class="lineNumber">17</div>
                 <div class="lineNumber">18</div>
                 </div>
                 <div class="editor php">
                 <div class="normal">
                 <div class="line"><span class="boundarymaker">&lt;?php</span></div>
                 <div class="line"></div>
                 <div class="line"><span class="linecomment">// This is a single-line comment</span></div>
                 <div class="line"><span class="variable">$stringvariable</span> = <span class="string">"examplestring"</span>;</div>
                 <div class="line"><span class="variable">$intvariable</span> = <span class="number">300</span>;</div>
                 <div class="line"></div>
                 <div class="line"><span class="comment">/**</span></div>
                 <div class="line"><span class="comment">&nbsp;* This is a multi-line comment </span></div>
                 <div class="line"><span class="comment">&nbsp;*&nbsp;</span><span class="phpdoc">@author</span><span class="comment"> Roger Dudler</span></div>
                 <div class="line"><span class="comment">&nbsp;*/</span></div>
                 <div class="line"><span class="keyword">class</span> <span class="class">Testclass</span> {</div>
                 <div class="line"></div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">function</span> test(<span class="variable">$param</span> = <span class="string">"default"</span>) {</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="keyword">return</span> <span class="keyword">true</span>;</div>
                 <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;}</div>
                 <div class="line"></div>
                 <div class="line">}</div>
                 <div class="line"></div>
                 <div class="line"><span class="boundarymaker">?&gt;</span></div>
                 </div>
                 </div>
                 <div class="clear"></div>
                 </div>
    <!--html-->
    <div class="title"> html代码演示</div>
    <div class="view">
            <div class="sidebar">
                <div class="lineNumber">0</div>
                <div class="lineNumber">1</div>
                <div class="lineNumber">2</div>
                <div class="lineNumber">3</div>
                <div class="lineNumber">4</div>
                <div class="lineNumber">5</div>
                <div class="lineNumber">6</div>
                <div class="lineNumber">7</div>
                <div class="lineNumber">8</div>
                <div class="lineNumber">9</div>
            </div>
            <div class="editor html">
                <div class="normal">
                    <div class="line"><span class="declBoder">&lt;</span><span class="doctypeExternalId">!DOCTYPE</span> <span class="doctypeName">html</span><span class="declBoder">&gt;</span></div>
                    <div class="line"><span class="tagborder">&lt;</span><span class="tag">html</span><span class="tagborder">&gt;</span></div>
                    <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="tagborder">&lt;</span><span class="tag">head</span><span class="tagborder">&gt;</span></div>
                    <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tagborder">&lt;</span><span class="tag">link</span> <span class="attribute">rel</span><span class="equals">=</span><span class="attributevalue">"stylesheet"</span> <span class="attribute">type</span><span class="equals">=</span><span class="attributevalue">"text/css"</span> <span class="tagborder">/&gt;</span></div>
                    <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="tagborder">&lt;/</span><span class="tag">head</span><span class="tagborder">&gt;</span></div>
                    <div class="line"><span class="tagborder">&lt;</span><span class="tag">body</span><span class="tagborder">&gt;</span></div>
                    <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="commentBorder">&lt;!--</span><span class="comment"> This is a comment </span><span class="commentBorder">--&gt;</span></div>
                    <div class="line">&nbsp;&nbsp;&nbsp;&nbsp;<span class="tagborder">&lt;</span><span class="tag">p</span><span class="tagborder">&gt;</span>three<span class="entity">&amp;amp;</span><span class="entity">&amp;amp;</span><span class="entity">&amp;amp;</span>spaces<span class="tagborder">&lt;/</span><span class="tag">p</span><span class="tagborder">&gt;</span></div>
                    <div class="line"><span class="tagborder">&lt;/</span><span class="tag">body</span><span class="tagborder">&gt;</span></div>
                    <div class="line"><span class="tagborder">&lt;/</span><span class="tag">html</span><span class="tagborder">&gt;</span></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    <div style="height:3em;"></div>
</div>
@endsection
