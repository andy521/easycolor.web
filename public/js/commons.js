/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * string  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * string  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
String.prototype.replaceAll  = function(s1,s2){
    return this.replace(new RegExp(s1,"gm"),s2);
}

String.prototype.nullOrEmpty = function(){
    return this == null || this == '';
}

//扩展数组方法
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * date * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * date * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.format = function(fmt)
{
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt))
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o)
        if(new RegExp("("+ k +")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
    return fmt;
}

/************************************************************************************************
* 常用js函数
************************************************************************************************/
//是否为null或者空字符串
function nullOrEmpty(s){
  return s == null || s == '' || s == 'null';
}

//手机号码合法性验证
function validPhone(phone){
    return /^1[34578]\d{9}$/.test(phone);
}

//填充div中的input元素
function fillForm(formId,entity){
    var inputs = $('#' + formId).find('input');
    var input,name;
    for (var i = 0 ; i < inputs.length ; i++) {
        input = $(inputs[i]);
        name = input.attr('name');
        input.val(entity[name]);
    }
}

//清空div中的input元素值
function clearForm(id) {
    var inputs = $('#' + id).find('input');
    for (var i = 0 ; i < inputs.length ; i++) {
        $(inputs[i]).val('');
    }
}

//将form中的值pick到一个object中
function pickEntity(id) {
    var r = {};
    var inputs = $('#' + id).find('input');
    var name;
    var input;
    for (var i = 0 ; i < inputs.length ; i++) {
        input = $(inputs[i]);
        name = input.attr('name');
        if(!nullOrEmpty(input.val()))
            r[name] = input.val();
    }
    return r;
}

//format .net json datetime
//.net mvc 序列化的日期格式类似于/Date(xxxxxxx)/
//使用本方法将此数据parse为Date对象
function parseNetJsonDateTime(val) {
    if (val == null)
        return null;
    return new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
}

/************************************************************************************************
 *										jquery的薄封装
************************************************************************************************/
//对指定id的元素取值和赋值
function val(id , val){
    if(arguments.length == 1)
        return $('#'+id).val();
    return $('#'+id).val(val);
}

//隐藏指定id的元素
function hide(id){
    $('#'+id).hide();
}

//显示指定id的元素
function show(id){
    $('#'+id).show();
}

/************************************************************************************************
 *										easyui的薄封装
************************************************************************************************/

//显示指定的dialog
function openDialog(id , title) {
    $('#'+id).dialog('open').dialog('setTitle',title);
}

//关闭dialog
function closeDialog(id){
    $('#'+id).dialog('close')
}

//enableButton 按钮可用
function enableButton(id){
    $('#' + id).linkbutton('enable');
}

//disableButton 按钮禁用
function disableButton(id){
    $('#' + id).linkbutton('disable');
}
