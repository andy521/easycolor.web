function renderList(data,listId,template) {
    if(nullOrEmpty(listId) || nullOrEmpty(template) || nullOrEmpty(data))
        console.log('参数不能为空');
    var l =  $('#'+listId);
    l.empty();
    var content = '';
    for(var i = 0 ; i < data.length ; i ++){
        var item = data[i];
        var temp = template;
        for(var key in item){
            temp = temp.replaceAll("{"+ key +"}",item[key]);
        }
        content += temp;
    }
    l.append(content);
}

function renderPageBar(pageData,pageId,callBackName) {
    //计算总页数
    var temp = pageData.total % pageData.size;
    var pageCount;
    if(temp == 0)
        pageCount = parseInt(pageData.total / pageData.size);
    else
        pageCount = parseInt(pageData.total / pageData.size) + 1;
    //指定查询函数,默认为find函数
    //相当于使用反射调用函数,这种方式不好，最好使用回调函数，暂时这样实现
    if(callBackName == null)
        callBackName = 'find';
    //模板
    var start = "<span href='#'  class='page-btn' onclick='"+callBackName+"(1)'>首页</span>\n";
    var end = "<span href='#'  class='page-btn' onclick='"+callBackName+"({index})'>尾页</span>\n";
    var up = "<span href='#' class='page-btn' onclick='"+ callBackName +"({index})'>上一页</span>\n";
    var down = "<span  href='#' class='page-btn' onclick='"+callBackName+"({index})'>下一页</span>\n";
    var ran = new Date().getTime();
    var go = "<input class='pageGo' id='go"+ ran +"'  value='{index}'/><span class='left' style='padding-top: .2em;'>/{pageCount}页</span>";

    var content = '';
    //首页和上一页
    if(pageData.index > 1) {
        content += start;
        up = up.replaceAll("{index}" , pageData.index - 1);
        content += up;
    }

    go = go.replaceAll("{index}",pageData.index);
    go = go.replaceAll("{pageCount}",pageCount);
    content += go;

    //尾页和下一页
    if(pageData.index < pageCount) {
        down = down.replaceAll("{index}", pageData.index + 1);
        content += down;
        end = end.replaceAll("{index}", pageCount);
        content += end;
    }

    content += "<span class='left' style='padding-top: .2em;'>&nbsp;共"+pageData.total+"个</span>";
    if(pageId == null)//默认pageBarId
        pageId = 'pagebar';
    var pageBar = $('#'+pageId);
    pageBar.empty();
    pageBar.append(content);
    //将pagebar居中
    var bar = $('#'+pageId);
    var btnCount =  bar.children().length;
    bar.css("width",(btnCount * 65) + "px");
    bar.css('margin',"0 auto");
    //监听页码回车事件
    $('#go'+ran).bind('keydown',function(event){
        if(event.keyCode == "13") {
            var temp = "{findFn}(val('{id}'))";
            temp = temp.replaceAll("{findFn}",callBackName);
            temp = temp.replaceAll("{id}","go"+ran);
            eval(temp);
        }
    });
}

//ckeditor 工具方法
function getEditor(fieldName,imgUploadUrl){
    var o = {
        language: 'zh-cn',
        height: 450
    };
    if(!nullOrEmpty(imgUploadUrl)){
        o.filebrowserUploadUrl = imgUploadUrl +'?_token=' + $('meta[name="csrf-token"]').attr('content')
    }
    return CKEDITOR.replace(fieldName, o);
}
