<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index.html', function () {
    return view('index');
});

Route::get('/intelliJidea/list.html','IdeaController@listView');
Route::get('/intelliJidea/detail/{id}.html','IdeaController@detail');
Route::post('/intelliJidea/find','IdeaController@find');

Route::get('/vim/list.html','VimController@listView');
Route::get('/vim/detail/{id}.html','VimController@detail');
Route::post('/vim/find','VimController@find');

Route::get('/eclipse/list.html','EclipseController@listView');
Route::get('/eclipse/detail/{id}.html','EclipseController@detail');
Route::post('/eclipse/find','EclipseController@find');

Route::get('/article/edit/{id?}','ArticleController@editView');
Route::post('/article/edit','ArticleController@edit');
Route::get('/article/{id}','ArticleController@detail');

Route::get('/download/idea/{link}','DownloadController@downloadIdea');
Route::get('/download/eclipse/{type}/{name}/{link}','DownloadController@downloadEclipse');

Route::post('/ckeditor/uploadImg','CkeditorController@uploadImg');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/v/check', 'VersionController@check');
