# easycolor.web

## 简介 

- 本项目已经上线，地址为：http://easycolor.cc
- 爬虫部分使用java实现，项目地址为：https://gitee.com/yanwushu/easycolor
- 这个web项目只是实现展示、搜索、下载服务。
- 代码很简单，比较适合入门的同学看下，由于精力有限，无法继续开发，有兴趣的同学可以在当前基础上开发和完善。
- 本人的联系方式为yanwushu@163.com

#easycolor.web
easycolor的web部分 使用laravel实现
