<?php

namespace App\Http\Controllers;

use App\Dao\ColorScheme;
use App\Utils\Page;
use Illuminate\Http\Request;

class VimController extends Controller
{
    private $colorSchemeDao;

    public function __construct(){
        $this->colorSchemeDao = new ColorScheme();
    }

    //vim list page
    public function listView(){
        return view('vimList');
    }

    //select one page data with condition
    public function find(Request $req){
        $page = Page::getPageFromReq($req);
        $name = $req->input('name');
        return response()->json($this->colorSchemeDao->find($page,$name,2));
    }

    //get a detail page
    public function detail($id){
        $bean =  $this->colorSchemeDao->get($id);
        return view('vimDetail',['bean'=>$bean]);
    }
}
