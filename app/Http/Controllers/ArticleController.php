<?php

namespace App\Http\Controllers;

use App\Dao\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $dao;

    public function __construct(){
        $this->dao= new Article();
    }

    //edit View
    public function editView($id = 0){
        if($id <= 0) {//create
            return view('articleEdit',['bean'=>'null']);
        }else{//edit
            $bean = $this->dao->get($id);
            return view('articleEdit', ['bean'=>$bean]);
        }
    }

    //edit
    public function edit(Request $req){
        $id = $req->input('id');
        //todo 如何判断插入是否成功？
        if(!$id)
            return Article::query()->create($req->all());
        return Article::query()->where('id',$id)->update($req->all());
    }

    //detail
    public function detail($id){
        $bean = $this->dao->get($id);
        return view('articleDetail',['bean'=>$bean]);
    }
}
