<?php

namespace App\Http\Controllers;

use App\Dao\ColorScheme;
use App\Dao\User;
use App\Utils\Page;
use Illuminate\Http\Request;

//testin version update
class VersionController extends Controller
{
    //check version info
    public function check(){
        return ['name'=>'/public/testin-0.1.21.apk','code'=>10];
    }
}
