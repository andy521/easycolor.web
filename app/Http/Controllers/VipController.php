<?php

namespace App\Http\Controllers;

use App\Dao\VipUser;
use Illuminate\Http\Request;

class VipController extends Controller
{
    private $dao;

    public function __construct(){
        $this->dao = new VipUser();
    }

    function auth(Request $req){
        return VipUser::auth($req->input("code"));
    }

    function renewView(){
        return view('renew');
    }

    function renew(Request $req){
        $r = VipUser::renew($req->input("code"),$req->input('key'));
        if($r)
            return ["success"=>true];
        return ["success"=>false];
    }

    //播放视频页面
    //http://easycolor.cc/vip/v/c57579466ae7d96fa15bb18dc6e4b1e7/aHR0cDovL3d3dy5pcWl5aS5jb20vdl8xOXJyN3AwanNzLmh0bWw=
    function v($md5,$url){
        $url = str_replace("_","/",$url);//url safe
        VipUser::validate($md5);
        return view("v",['url'=>base64_decode($url)]);
    }

    //余额不足，强强通道 'http://359995885.duapp.com/7353233630/?url='
    //那云平台： http://api.nepian.com/ckparse/?isckplayer=&isjquery=&url=
    //获取视频网站的通道列表
    function getAndroidService() {
        return base64_encode(base64_encode(base64_encode("{
            'youku' : [
                 'm16',
                 'http://api.nepian.com/ckparse/?isckplayer=&isjquery=&url=',
                 'http://jiexi.92fz.cn/player/vip.php?url='
            ],
            'letv' : [
                'm16',
                'http://www.ou522.cn/t2/1.php?url='
            ],
            'iqiyi' : [
                'm16',
                'http://014670.cn/jx/yk.php?url='
            ],
            'tencent' : [
                'm16',
                'http://y.mt2t.com/lines?url=',
                'http://jiexi.92fz.cn/player/vip.php?url='
            ],
            'sohu' : [
                'm16',
                'http://api.30pan.com/api/?url=',
                'http://www.82190555.com/video.php?url='
            ],
            'mgtv' : [
                'm16',
                'http://player.quankan.tv/playm3u8/index.php?url=',
                'http://www.ou522.cn/t2/1.php?url='
            ],
            'default' : [
                'm16',
                'http://www.ou522.cn/t2/1.php?url='
            ]
        }")));
    }

    //获取视频网站的通道列表
    function getService(){
        return [
            'youku'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
            ],
            'letv'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://000o.cc/jx/yk.php?url=',
                'http://2.jx.72du.com/video.php?url=',
                'http://65yw.2m.vc/chaojikan.php?url='
            ],
            'iqiyi'=>[
                'http://api.wlzhan.com/sudu/?url=',
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'http://vip.jlsprh.com/jiexi/47ks/?url=',
                'http://www.ou522.cn/t2/1.php?url=' ,
                'm16'
            ],
            'tencent'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'http://www.82190555.com/video.php?url=',
                'http://2.jx.72du.com/video.php?url=',
                'm16'
            ],
            'sohu'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'tudou'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://www.wmxz.wang/video.php?url='
            ],
            'mgtv'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'a'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'b'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'fengxin'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'huashu'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'y56'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'yinyue'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'y1905'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'pptv'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16',
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'bf'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ],
            'default'=>[
                'http://jiexi.92fz.cn/player/vip.php?url=',
                'm16'
                ,
                'http://j.zz22x.com/jx/?url=',
                'http://www.16891699.com/index/youku.php?url='
            ]
        ];
    }
}
