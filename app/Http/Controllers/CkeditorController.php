<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class CkeditorController extends Controller
{
    public function uploadImg(Request $req){
        $file = $req->file('upload');
        if(!$file)
            return false;
        $fileName = Carbon::now()->timestamp.'.'.$file->extension();
        $path = public_path('upload\ckeditorImg');
        $file->move($path,$fileName);
        return '<script>window.parent.CKEDITOR.tools.callFunction("'.$req->input('CKEditorFuncNum').'","/upload/ckeditorImg/'.$fileName.'");</script>';
    }
}
