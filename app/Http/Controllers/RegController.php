<?php

namespace App\Http\Controllers;

use App\Dao\RegAccount;
use App\Dao\RegSite;
use App\Dao\RegUser;
use App\Dao\Util;
use Illuminate\Http\Request;

class RegController extends Controller
{
    public function __construct(){
    }

    function auth(Request $req){
        $r = null;
        if(!Util::validateSign($req->all())) {
            $r = ["success"=>false,"msg"=>"此路不通，换个思路破解吧 :( "];
            return $r;
        } else{
            $r = RegUser::auth($req->input("code"));
            $r['sign'] = Util::backSign($req->all());
            return $r;
        }
    }

    function renewView(){
        return view('renew');
    }

    function renew(Request $req){
        if(!Util::validateSign($req->all())) {
            $r = ["success"=>false,"msg"=>"此路不通，换个思路破解吧 :( "];
            return $r;
        }
        $temp = RegUser::renew($req->input("code"),$req->input('key'));
        $r = null;
        if($temp) {
            $r = ["success"=>true];
        } else{
            $r = ["success"=>false];
        }
        $r['sign'] = Util::backSign($req->all());
        return $r;
    }

    //保存账户信息
    function collectionAccount(Request $req){
        if(!Util::validateSign($req->all()))
            return ["success"=>false,"msg"=>"此路不通，换个思路破解吧 :( "];
        $r = RegAccount::collectionAccount($req->all());
        if($r)
            return ["success"=>true];
        return ["success"=>false];
    }

    //网站列表
    function data(Request $req){
        $r = null;
        if(!Util::validateSign($req->all())) {
            $r = ["success"=>false,"msg"=>"此路不通，换个思路破解吧 :( "];
            return $r;
        }
        $r['data'] = RegSite::query()->get();
        $r['sign'] = Util::backSign($req->all());
        return $r;
    }
}
