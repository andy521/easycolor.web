<?php

namespace App\Http\Controllers;

use App\Dao\ColorScheme;
use App\Dao\User;
use App\Utils\Page;
use Illuminate\Http\Request;

//idea scheme controller
class IdeaController extends Controller
{

    private $dao;

    public function __construct(){
        $this->dao = new ColorScheme();
    }

    //idea list page
    public function listView(){
        return view('ideaList');
    }

    //select one page data with condition
    public function find(Request $req){
        $page = Page::getPageFromReq($req);
        $name = $req->input('name');
        return response()->json($this->dao->find($page,$name,5));
    }

    //get a detail page
    public function detail($id){
        $bean =  $this->dao->get($id);
        return view('ideaDetail',['bean'=>$bean]);
    }
}
