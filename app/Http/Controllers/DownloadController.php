<?php

namespace App\Http\Controllers;


class DownloadController extends Controller
{

    public function downloadIdea($link=null){
        if($link == null) {
            return view('error.404');
        }
        $file = public_path().'/scheme/'.$link.'.jar';
        $headers = array( 'Content-Type: application/jar', );
        return response()->download($file, $link.'.jar', $headers);
    }

    public function downloadEclipse($type = null,$name = null, $link=null){
        if($type == null || $link == null || $name == null) {
            return view('error.404');
        }
        $file = public_path().'/scheme/eclipse/color_scheme_'.$link.'.'.$type;
        $headers = array( 'Content-Type: application/'.$type, );
        return response()->download($file, $name.'.'.$type, $headers);
    }
}
