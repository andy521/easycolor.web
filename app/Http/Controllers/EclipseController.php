<?php

namespace App\Http\Controllers;

use App\Dao\ColorScheme;
use App\Dao\Demo;
use App\Utils\Page;
use Illuminate\Http\Request;

class EclipseController extends Controller {

    private $colorSchemeDao;
    private $demoDao;

    public function __construct(){
        $this->colorSchemeDao = new ColorScheme();
        $this->demoDao= new Demo();
    }

    //vim list page
    public function listView(){
        return view('eclipseList');
    }

    //select one page data with condition
    public function find(Request $req){
        $page = Page::getPageFromReq($req);
        $name = $req->input('name');
        return response()->json($this->colorSchemeDao->find($page,$name,3));
    }

    //get a detail page
    public function detail($id){
        $bean =  $this->colorSchemeDao->get($id);
        $demos  = $this->demoDao->getBySchemeId($id);
        return view('eclipseDetail',
            [
                'bean'=>$bean,
                'demos'=>$demos
            ]
        );
    }
}
