<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;


class Demo extends Model
{
    protected $table = 'demo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    //demos of one scheme
    public  function getBySchemeId($schemeId){
        return Demo::query()->where('scheme_id',$schemeId)->get();
    }
}
