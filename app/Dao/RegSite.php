<?php

namespace App\Dao;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RegSite extends Model
{
    public $timestamps = false;
    protected $table = 'reg_site';

    protected $fillable = [ 'url', 'typez','remark','reg_url','bank_url','offset'];
}
