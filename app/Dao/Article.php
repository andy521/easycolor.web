<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'article';
    protected $guarded= [];

    public function get($id){
        return Article::query()->find($id);
    }
}
