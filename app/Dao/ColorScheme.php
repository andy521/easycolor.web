<?php

namespace App\Dao;

use App\Utils\Page;
use App\Utils\Resp;
use Illuminate\Database\Eloquent\Model;


class ColorScheme extends Model
{
    protected $table = 'color_scheme';
    protected $primaryKey = 'id';
    public $timestamps = false;

    //find with condition
    public function find(Page $page,$name,$type){
        $resp = new Resp();
        $query = ColorScheme::query()->where('type_id',$type);
        if($name != null)
           $query = $query ->where('name','like','%'.$name.'%');
        $page->total = $query->count();//count和get的调用有顺序性
        $resp->data = $query
            ->skip($page->getOffset())
            ->limit($page->size)->get();
        $resp->page = $page;
        return $resp;
    }

    //get the detail of a colorScheme
    public function get($id){
        return ColorScheme::query()->find($id);
    }
}
