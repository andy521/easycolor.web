<?php

namespace App\Dao;

class Util
{
    //验证sign
    public static function validateSign(array $p){
        $sign = $p['sign'];
        return $sign == self::sign($p);
    }

    //签名规则 md5(timestamp + user_id + user_id + 1);
    public static function sign(array $p){
        $timestamp = $p['timestamp'];
        $user_id = $p['user_id'];
        $a = $timestamp . $user_id. $user_id .  "1";
        return hash('md5',$a);
    }

    //返回签名规则 md5(timestamp + user_id + user_id + user_id + 1);
    public static function backSign(array $p){
        $timestamp = $p['timestamp'];
        $user_id = $p['user_id'];
        $a = $timestamp . $user_id. $user_id . $user_id . "1";
        return hash('md5',$a);
    }
}
