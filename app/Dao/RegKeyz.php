<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;

class RegKeyz extends Model
{
    public $timestamps = false;
    protected $table = 'reg_keyz';
    protected $guarded= [];
}
