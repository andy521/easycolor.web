<?php

namespace App\Dao;

use Illuminate\Database\Eloquent\Model;

class RegAccount extends Model
{
    public $timestamps = false;
    protected $table = 'reg_account';
    protected $fillable = ['name', 'pass','account','phone','wechat','qq','province','country','bank_account','bank_name','user_id'];

    //记录账户信息
    public static function collectionAccount(array $params){
        $a = RegAccount::query()
            ->where('name',$params['name'])
            ->where('bank_account',$params['bankAccount'])
            ->count();
        if($a > 0)
            return true;
       return RegAccount::query()->create($params);
    }
}
