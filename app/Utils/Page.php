<?php

namespace App\Utils;

use Illuminate\Http\Request;

/**
 * User: yanwushu
 * Date: 2017/8/3
 * Time: 20:37
 */
class Page
{
    public $index;
    public $size;
    public $total;//all data count

    //make a instance of page from httpRequest
    public static function getPageFromReq(Request $req){
        $r = new Page();
        if(!$req->has('index'))
            $r->index = 1;
        else
            $r->index = (int)$req->input('index');
        $r->size = 12;
        return $r;
    }

    public function getOffset(){
        return $this->size * ($this->index - 1);
    }
}